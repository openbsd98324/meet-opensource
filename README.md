# MEET Opensource


## Jitsi Meet 



My favorite Meet on Opensource platform is  [MEET JITSI Opensource, Click here](https://meet.jit.si/opensource).


Direct Link for Web Browser under Linux: 

````
 /usr/bin/firefox  https://meet.jit.si/opensource 
````






## Getting started with OpenSource 


Open source is source code that is made freely available for possible modification and redistribution. Products include permission to use the source code,[1] design documents,[2] or content of the product. The open-source model is a decentralized software development model that encourages open collaboration.[3][4] A main principle of open-source software development is peer production, with products such as source code, blueprints, and documentation freely available to the public. The open-source movement in software began as a response to the limitations of proprietary code. The model is used for projects such as in open-source appropriate technology,[5] and open-source drug discovery.[6][7]

Open source promotes universal access via an open-source or free license to a product's design or blueprint, and universal redistribution of that design or blueprint.[8][9] Before the phrase open source became widely adopted, developers and producers have used a variety of other terms. Open source gained hold with the rise of the Internet.[10] The open-source software movement arose to clarify copyright, licensing, domain, and consumer issues.

Generally, open source refers to a computer program in which the source code is available to the general public for use or modification from its original design. Code is released under the terms of a software license. Depending on the license terms, others may then download, modify, and publish their version (fork) back to the community.

Link: https://en.wikipedia.org/wiki/Open_source




## Join the Online Meeting 


![](media/1637486606-screenshot.png)

![](media/screenshot-1637479872.png)


## Hardware and Software 

2021,
Meet Opensource was tested on OpenSuse LEAP 15.3 AMD64, running
on Ryzen 3, AMD64.


Kernel:
Linux localhost.localdomain 5.3.18-57-default #1 SMP Wed Apr 28 10:54:41 UTC 2021 (ba3c2e9) x86_64 x86_64 x86_64 GNU/Linux


firefox --version: 
Mozilla Firefox 91.3.0esr


My favorite search engine is [Duck Duck Go](https://duckduckgo.com).


## References


1.)  Link: https://en.wikipedia.org/wiki/Open_source

2.)  Link: www.gnu.org

3.)  Link: www.linux.org

4.)  Link: www.ircnow.org 




